import util.Calculations;

import javax.swing.*;

/**
 * Created by adam on 04.12.16.
 */
public class FeatureSelection {

    /**
     * Selekcja cech w przestrzeni 1D.
     */
    public static void selectFeaturesBy1DFisherCriterion(int featureCount, JLabel l_FLD_winner, JLabel l_FLD_val, double[][] F, int[] sampleCount, int[] classLabels) {
        double FLD = 0, tmp;
        int max_ind = -1;
        for(int i = 0; i < featureCount; i++){
            if((tmp = Calculations.computeFisherLD(F[i], sampleCount, classLabels)) > FLD){
                FLD = tmp;
                max_ind = i;
            }
        }
        l_FLD_winner.setText(max_ind + "");
        l_FLD_val.setText(FLD + "");
    }
}
