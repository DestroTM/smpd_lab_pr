/**
 * Created by Adam on 04.12.2016.
 */
public class FisherFetaureSomething {

    public double[] featureVector;
    public double fisher;
    public int featureId;

    public FisherFetaureSomething() {

    }

    public FisherFetaureSomething(double[] featureVector, double fisher, int featureId) {
        this.featureVector = featureVector;
        this.fisher = fisher;
        this.featureId = featureId;
    }
}
