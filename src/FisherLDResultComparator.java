import java.util.Comparator;

/**
 * Created by Adam on 04.12.2016.
 */
public class FisherLDResultComparator implements Comparator<FisherFetaureSomething> {

    @Override
    public int compare(FisherFetaureSomething f1, FisherFetaureSomething f2) {
        if (f1.fisher < f2.fisher) return -1;
        if (f1.fisher > f2.fisher) return 1;
        return 0;
    }
}