package util;

import Jama.Matrix;

/**
 * Klasa rozszerzajaca klase macierzy.
 * 
 * @author adam
 */
public class MatrixExtended extends Matrix {   

    public MatrixExtended(double[][] doubles) {
        super(doubles);
    }
    
    /**
     * Wyświetlanie macierzy w konsoli.
     */
    public void printMatrix() {
        for (double[] vec : this.getArray()) {
            System.out.print("[ ");
            for (double var : vec) {
                System.out.print(var + " ");
            }
            System.out.println("]");
        }
        System.out.println("");
    }
    
    /**
     * Wyświetlanie macierzy w konsoli.
     * 
     * @param matrix macierz
     */
    public static void printMatrix(double[][] matrix) {
        for (double[] vec : matrix) {
            System.out.print("[ ");
            for (double var : vec) {
                System.out.print(var + " ");
            }
            System.out.println("]");
        }
        System.out.println("");
    }
    
    /**
     * Wyświetlanie macierzy w konsoli.
     * 
     * @param matrix macierz
     */
    public static void printMatrix(Matrix matrix) {
        printMatrix(matrix.getArray());
    }
    
    /**
     * Metoda replikuje wektory w celu utworzenia macierzy odpowiedniej dlugosci.
     * @param vector wektor
     * @param matrixLength ilosc wektorow w macierzy
     * @return macierz
     */
    public static double[][] replicateVectorToMatrix(double[] vector, int matrixLength) {
        double[][] matrix = new double[vector.length][matrixLength];
        
        for (int i = 0; i < vector.length; i++) {
            for (int j = 0; j < matrixLength; j++) {
                matrix[i][j] = vector[i];
            }
        }
        
        return matrix;
    }
    
    /**
     * Metoda filtruje macierz na podstawie klasy.
     * @param featureMatrix macierz cech
     * @param classLabels labelki klas
     * @param classLabel wybrana klasa
     * @return odfiltrowana macierz
     */
    public static double[][] filterMatrixByClass(double[][] featureMatrix, int[] classLabels, int classLabel) {
        int labelCount = 0;
        for(int label : classLabels) {
            if (label == classLabel) {
                labelCount++;
            }
        }
        
        double[][] classMatrix = new double[featureMatrix.length][labelCount];
        
        for (int i = 0, iN = 0; i < featureMatrix.length; i++) {
            for (int j = 0, jN = 0; j < featureMatrix[i].length; j++) {
                if (classLabels[j] == classLabel) {
                    classMatrix[iN][jN] = featureMatrix[i][j];
                    jN++;
                }
            }
            iN++;
        }
        
        return classMatrix;
    }
}