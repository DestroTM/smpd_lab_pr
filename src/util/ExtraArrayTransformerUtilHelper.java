/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.List;

/**
 * Transformacje listowo obiektowo tablicowo prymitwne
 * @author nie znam
 */
public class ExtraArrayTransformerUtilHelper {
    
    /**
     * To musialo sie stac...
     * @param list lista Integerow
     * @return tablica intów
     */
    public static int[] toIntArray(List<Integer> list) {
        int[] ret = new int[list.size()];
        int i = 0;
        for (Integer e : list) {
            ret[i++] = e.intValue();
        }
        return ret;
    }   
}
