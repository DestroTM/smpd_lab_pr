package util;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author adam
 */
public class Calculations {
    
    /**
     * Oblicza współczynnik Fishera dla przestrzeni n-D.
     * @param featuresGroup macierz cech
     * @param classLabels labelki do odroznienia czy cecha jest z klasy A czy B
     * @return współczynnik Fishera dla przestrzeni n-D.
     */
    public static double computeFisherND(double[][] featuresGroup, int[] classLabels) {
        // nD, 2-classes
        
        // macierz group cech klas A i B
        double[][] featuresGroupForClassA = MatrixExtended.filterMatrixByClass(featuresGroup, classLabels, 0);
        double[][] featuresGroupForClassB = MatrixExtended.filterMatrixByClass(featuresGroup, classLabels, 1);
                
        // wektor srednich klasy A i B
        double[] averagesOfFeatureForClassA = computeAverage(featuresGroupForClassA);
        double[] averagesOfFeatureForClassB = computeAverage(featuresGroupForClassB);
        
        // macierz rozrzutu klasy A i B
        MatrixExtended scatterboxMatrixForClassA = calculateScatterboxMatrix(featuresGroupForClassA, averagesOfFeatureForClassA);
        MatrixExtended scatterboxMatrixForClassB = calculateScatterboxMatrix(featuresGroupForClassB, averagesOfFeatureForClassB);
        
        // odleglosc
        double distance = calculateEuclidesDistance(averagesOfFeatureForClassA, averagesOfFeatureForClassB);
        
        // wyznaczniki macierzy rozrzutu
        double detA = scatterboxMatrixForClassA.det();
        double detB = scatterboxMatrixForClassB.det();
        
        // współczynnik fishera
        double fisherFactor = distance / (detA + detB);
        
        return fisherFactor;
    }
    
    /**
     * Oblicza srednia arytmetyczna dla cech danej klasy i umieszcza ją w wektorze.
     * @param featuresGroup macierz pogrupowanych cech
     * @param classLabels labelki do odroznienia czy cecha jest z klasy A czy B
     * @param classLabel 0/1 - liczone tylko dla klasy A/B
     * @return wektor srednich dla cech
     */
    public static double[] computeAverage(double[][] featuresGroup) {
        double[] averageVector = new double[featuresGroup.length];
        
        for (int i = 0; i < featuresGroup.length; i++) {
            double sum = 0; int count = 0;
            for (int j = 0; j < featuresGroup[i].length; j++) {
                sum += featuresGroup[i][j];
                count++;            
            }
            averageVector[i] = sum / count;
        }
        
        return averageVector;
    }
    
    /**
     * Oblicza macierz rozrzutu.
     * 
     * @param featuresGroup macierz zgrupowanych cech danej klasy
     * @param averageVector wektor srednich wartosci cech klasy
     * @return macierz rozrzutu
     */
    public static MatrixExtended calculateScatterboxMatrix(double[][] featuresGroup, double[] averageVector) {
        MatrixExtended averageMatrix = new MatrixExtended(MatrixExtended.replicateVectorToMatrix(averageVector, featuresGroup[0].length));
        System.out.println("\n\naverageMatrix");
        averageMatrix.printMatrix();
        MatrixExtended featuresGroupMatrix = new MatrixExtended(featuresGroup);
        System.out.println("\n\nfeaturesGroupMatrix");
        featuresGroupMatrix.printMatrix();

        MatrixExtended substractionMatrix = new MatrixExtended(featuresGroupMatrix.minus(averageMatrix).getArray());
        MatrixExtended transposeMatrix = new MatrixExtended(substractionMatrix.transpose().getArray());
        
        MatrixExtended scatterboxMatrix = new MatrixExtended(substractionMatrix.times(transposeMatrix).getArray());
                
        return scatterboxMatrix;
    }
    
    /**
     * Oblicza odległość Euklidesową.
     * 
     * @param avgA wektor srednich cech klasy A
     * @param avgB wektor srednich cech klasy B
     * @return odleglosc
     */
    public static double calculateEuclidesDistance(double[] avgA, double[] avgB) {
        int size = avgA.length;
                
        double avgSumPoweredSubstraction = 0;
        
        for (int i = 0; i < size; i++) {
            avgSumPoweredSubstraction += Math.pow(avgA[i] - avgB[i], 2);
        }
        
        return Math.sqrt(avgSumPoweredSubstraction);
    }

    /**
     * Współczynnik fishera w przestrzeni jednowymiarowej.
     * @param vectorOfFeatures wektor cech
     * @return fisher
     */
    public static double computeFisherLD(double[] vectorOfFeatures, int[] sampleCount, int[] classLabels) {
        // 1D, 2-classes
        double mA = 0, mB = 0, sA = 0, sB = 0;

        for(int i = 0; i < vectorOfFeatures.length; i++){
            if(classLabels[i] == 0) { //rozrozniamy czy obiekt jest klasy A czy B
                mA += vectorOfFeatures[i]; //suma wartosci cech obiektow klasy A
                sA += vectorOfFeatures[i] * vectorOfFeatures[i]; //Tu liczymy E(X)?
            }
            else {
                mB += vectorOfFeatures[i]; //suma wartosci cech obiektow klasy B
                sB += vectorOfFeatures[i] * vectorOfFeatures[i];
            }
        }
        mA /= sampleCount[0]; //obliczamy srednia cech obiektow klasy A
        mB /= sampleCount[1]; //obliczamy srednia cech obiektow klasy B
        sA = sA/sampleCount[0] - mA*mA; //nie rozumiem czemu tak... Zgaduje ze to odchylenie standardowe^2
        sB = sB/sampleCount[1] - mB*mB;

        //zwracany wspolczynnik Fishera
        return Math.abs(mA-mB)/(Math.sqrt(sA)+Math.sqrt(sB));
    }
}