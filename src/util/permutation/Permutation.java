package util.permutation;

import combinatorics.ChoiceIterable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import util.ExtraArrayTransformerUtilHelper;

/**
 *
 * @author adam
 */
public class Permutation {
    
    /**
     * Rekurencyjna metoda służąca do znalezienia wszystkich kombinacji wektorów, bez powtórzeń.
     * 
     * @param featureMatrix Macierz cech
     * @param tempFeatureMatrix Macierz pomocnicza do zapisywania wybranych wektorów
     * @param start Indeks od którego rozpoczyna się kolejna iteracja
     * @param index Indeks wektora (prezentuje też 'poziom rekurencji'
     * @param n Ilość cech w wektorze
     * @param listOfFeaturesGroups Lista macierzy ze zgrupowanymi cechami
     */
    private static void combination(double featureMatrix[][], double tempFeatureMatrix[][], int start, int index, int n, List<double[][]> listOfFeaturesGroups, List<int[]> selectedFeatures, int[] tempSelectedFeatures) {
        
        System.out.println("Start: " + start + "index: " + index);
        
        // 'poziom rekurencji' jest równy ilosci cech wiec wypycham gotowa macierz
        if (index == n)
        {
            double[][] matrix = new double[n][featureMatrix[0].length];
            
            for (int i = 0; i < n; i++) {  
                matrix[i] = tempFeatureMatrix[i];
//                System.out.print("[ ");
//                for (int j = 0; j < tempFeatureMatrix[i].length; j++) {
//                    System.out.print(tempFeatureMatrix[i][j] + " ");
//                }
//                System.out.print("]");
            }
            
            listOfFeaturesGroups.add(matrix);
            selectedFeatures.add(Arrays.copyOf(tempSelectedFeatures, tempSelectedFeatures.length));
//            System.out.println("");
            return;
        }
        
        for (int i = start; i < featureMatrix.length; i++)
        {
            // zapisuje wybrany wektor w macierzy pomocniczej
            tempFeatureMatrix[index] = featureMatrix[i];
            
            // zapisuje indeksy cech znajdujacych sie w zestawie
            tempSelectedFeatures[index] = i;
                    
            combination(featureMatrix, tempFeatureMatrix, i + 1, index + 1, n, listOfFeaturesGroups, selectedFeatures, tempSelectedFeatures);
        }
    }
    
    public static FeaturesGroupsAndIndexes getCombinationList(double featureMatrix[][], int n)
    {
        // drukowanko - logowanie startu metody
        //System.out.println("getCombinationList | START");
        long start = System.currentTimeMillis();
        
        // Obiekt zawierajacy macierze pogrupowanych cech oraz liste wektorow z indeksami tych cech
        FeaturesGroupsAndIndexes featuresGroupsAndIndexes = new FeaturesGroupsAndIndexes();
        
        // Pomocnicza tablica do przechowywania wszystkich kombinacji
        double[][] temp = new double[n][featureMatrix[0].length];
        
        // Pomocnicza tablica do zapisywania indeksow cech. Aktualnie nieuzywana
        // ze wzgledu na nowa metodke z biblioteki niemca. Wykorzystane dla Permutation.combination
        // int[] tempSelectedFeatures = new int[n];
 
        // lista indeksow cech dla ponizszego wybieratora
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < featureMatrix.length; i++) {
            list.add(new Integer(i));
        }
        
        // nowa metoda wybierajaca wszystkie mozliwe grupy cech
        ChoiceIterable<Integer> iterable = new ChoiceIterable<Integer>(n, list);
        for (List<Integer> selectedIndexexOfFetaures : iterable) {
            
            // drukowanko wszystkich mozliwe grupy cech znalezionych przez algorytm niemca
            //System.out.println(selectedIndexexOfFetaures);
            
            double[][] matrixOfSelectedFeaturesForAllObjects = new double[selectedIndexexOfFetaures.size()][featureMatrix.length];
            
            for (int i = 0; i < selectedIndexexOfFetaures.size(); i++) {
                matrixOfSelectedFeaturesForAllObjects[i] = featureMatrix[selectedIndexexOfFetaures.get(i)];
            }
            featuresGroupsAndIndexes.getListOfFeaturesGroup().add(matrixOfSelectedFeaturesForAllObjects);
            featuresGroupsAndIndexes.getSelectedFeatures().add(ExtraArrayTransformerUtilHelper.toIntArray(selectedIndexexOfFetaures));
        }
        
        // Print all combination using temprary array 'data[]'
        // combination(featureMatrix, temp, 0, 0, n, featuresGroupsAndIndexes.getListOfFeaturesGroup(), featuresGroupsAndIndexes.getSelectedFeatures(), tempSelectedFeatures);
        
        // drukowanko zakonczenia metody i czasu jej dzialania
        //System.out.println("getCombinationList | END");
        //System.out.println("getCombinationList | Execution time: " + (System.currentTimeMillis() - start) / 1000.00);
        
        return featuresGroupsAndIndexes;
    }
    
    /**
     * Klasa testowa.
     * 
     * @param args argumenty.
     */
    public static void main (String[] args) {        
        double featureMatrix[][] = new double[5][2];
        
        featureMatrix[0][0] = 1;    featureMatrix[0][1] = 1;
        featureMatrix[1][0] = 2;    featureMatrix[1][1] = 2;
        featureMatrix[2][0] = 3;    featureMatrix[2][1] = 3;
        featureMatrix[3][0] = 4;    featureMatrix[3][1] = 4;
        featureMatrix[4][0] = 5;    featureMatrix[4][1] = 5;
       
        int n = 4;
        
        FeaturesGroupsAndIndexes featuresGroupsAndIndexes = getCombinationList(featureMatrix, n);
    }
}