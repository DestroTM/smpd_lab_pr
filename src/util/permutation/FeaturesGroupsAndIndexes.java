package util.permutation;

import java.util.ArrayList;
import util.MatrixExtended;

/**
 *
 * @author adam
 */
public class FeaturesGroupsAndIndexes {
    // Lista wybranych grup cech
    private ArrayList<double[][]> listOfFeaturesGroup = new ArrayList<double[][]>();

    // Indeksy wybranych cech w grupach z powyższej listy z zachowana kolejnoscia
    private ArrayList<int[]> selectedFeatures = new ArrayList<int[]>();

    public FeaturesGroupsAndIndexes() {
        
    }

    public ArrayList<double[][]> getListOfFeaturesGroup() {
        return listOfFeaturesGroup;
    }

    public void setListOfFeaturesGroup(final ArrayList<double[][]> listOfFeaturesGroup) {
        this.listOfFeaturesGroup = listOfFeaturesGroup;
    }

    public ArrayList<int[]> getSelectedFeatures() {
        return selectedFeatures;
    }

    public void setSelectedFeatures(final ArrayList<int[]> selectedFeatures) {
        this.selectedFeatures = selectedFeatures;
    } 
    
    public void printFeatureIndexesAndMatrix() {
        for (int i = 0; i < listOfFeaturesGroup.size(); i++) {
            System.out.print("FEATURES: "); 
            for (int index : selectedFeatures.get(i)) { 
                    System.out.print(index + " ");
            }
            System.out.println("");
            
            
            MatrixExtended matrix = new MatrixExtended(listOfFeaturesGroup.get(i));
            
            // drukowanko wykombinowanych macierzy
            // wykomentowane ze względu na duży spam w outpucie
            // matrix.printMatrix();
            System.out.println("");
        }
    }
}