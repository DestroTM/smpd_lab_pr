package util;

/**
 * Created by adam on 04.12.16.
 */
public class ExtraArrayOtherUtilHelper {

    public static double[][] macierzoKopiarka(double[][] oryginal) {
        double[][] kopia = new double[oryginal.length][oryginal[0].length];

        for (int i = 0; i < oryginal.length; i++) {
            System.arraycopy(oryginal[i], 0, kopia[i], 0, oryginal[i].length);
        }

        return kopia;
    }

    /**
     *
     * @param arr ostatni element w tym moze byc gówniakiem także jego nie sprawdzam, bo jeszcze nie jest ustawiony
     * @param val
     * @return
     */
    public static boolean arrayContainValue(int[] arr, int val) {
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] == val) {
                return true;
            }
        }
        return false;
    }
}
